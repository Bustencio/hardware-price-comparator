const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ComponentSchema = new Schema(
    {
        name: {type: String, required: true},
        category: {type: Schema.Types.ObjectId, ref: 'Category', required: true},
        price: {type: Boolean, required: true},
        brand: {type: String, required: true}
    }
)

ComponentSchema
.virtual('url')
.get(() => {
    return '/list/component/' + this.name;
});

module.exports = mongoose.model('Component', ComponentSchema);