const fs = require('fs');
const rp = require('request-promise');
const $ = require('cheerio');
const Component = require('../models/component');
const Category = require('../models/category');

const mainUrl = 'https://www.pccomponentes.com';


exports.scrape_start = (req, res, next) => {

    category = '/placas-base';
    url = mainUrl + category;

    const dataUrls = [];

    rp(url)
        .then((html) => {

            // Capturamos el elemento usando Cheerio
            links = $('.tarjeta-articulo__elementos-adicionales > a', html);
            $(links).each((i, link) => {
                dataUrls.push($(link).attr('href'));
            });

            console.log(dataUrls);

            return Promise.all(
                dataUrls.map((url) => {
                    rp(mainUrl + url)
                    .then((html) => {
                        /* let Component = new Component(
                            {
                                name: $('.articulo > h1', html).text(),
                                category: $('.navegacion-secundaria__migas-de-pan > a', html).last().text(),
                                price: $('.priceBlock', html).attr('data-price'),

                            }
                        ) */
                        console.log('Articulo: ' + $('.articulo > h1', html).text());
                        console.log('Categoroia: ' + $('.navegacion-secundaria__migas-de-pan > a', html).last().text())
                        console.log('Precio: ' + $('.priceBlock', html).attr('data-price'));
                    })
                })
            )

        })
        .catch((err) => {
            //handle error
        });
    
    
    
    res.redirect('/'); 
};