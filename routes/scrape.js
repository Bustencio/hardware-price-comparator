const express = require('express');
const router = express.Router();
const scrape_controller = require('../controller/scrape_controller');

router.get('/', scrape_controller.scrape_start);
  
module.exports = router;